const chrono = require('chrono-node');
const moment = require('moment-timezone');
const marked = require('marked');

const DATE_AND_TIME_FORMAT = 'ddd, MMM D HH:mm';
const ZONE_FORMAT = 'z';
const TIME_FORMAT = 'HH:mm';
const DATE_FORMAT = 'ddd, MMM D, YYYY HH:mm';
const CASUAL_FORMAT = 'ddd, MMM D';

// It will ignore the time parser for timezones that can be
// mistaken for English words written in lowercase only.
const AMBIGUOUS_TIMEZONE_REGEX = /(?<timezone>bot|cast|cat|eat|get|pet|vet|wet)/g;

// Disable zh-Hant support in the default chrono parser
chrono.casual.parsers = chrono.casual.parsers.filter((parser) => {
    return !parser.constructor.name.startsWith('ZHHant');
});

// Casual date (e.g. today, tomorrow...) does not have
// timezoneOffset and meridiem when located far from the
// hour+timezone in the message (e.g "Today, maybe at 1pm GMT")
//
// So this code overwrite the chrono parsers to add the is same
// timezoneOffset and meridiem of the hour to the casual date.
chrono.casual.refiners.push({
    refine: (context, results) => {
        const clearedResults = [];

        // console.log(results);
        results.forEach((result, index) => {
            if (result.text.match(AMBIGUOUS_TIMEZONE_REGEX)) {
                return;
            }

            if (result.start.isCertain('timezoneOffset') || results.length === 1) {
                clearedResults.push(result);
            } else {
                const timezoneOffset = results[index - 1] ?
                    results[index - 1].start.knownValues.timezoneOffset :
                    results[index + 1].start.knownValues.timezoneOffset;
                const meridiem = results[index - 1] ?
                    results[index - 1].start.knownValues.meridiem :
                    results[index + 1].start.knownValues.meridiem;

                result.start.assign('timezoneOffset', timezoneOffset);
                result.start.assign('meridiem', meridiem);
                clearedResults.push(result);
            }
        });
        return clearedResults;
    },
});

export function convertTimesToLocal(message, messageCreationTime, localTimezone, locale) {
    const filterRenderer = new marked.Renderer();
    filterRenderer.blockquote = () => '';
    filterRenderer.code = () => '';
    filterRenderer.codespan = () => '';
    filterRenderer.link = () => '';

    // The char "-" alone is not interpreted as a word by the parse, so it causes problems with multiple dates separated by it.
    const charIndex = message.indexOf('-');
    const clearedMessage = charIndex === -1 ? message : message.slice(0, charIndex) + '-' + message.slice(charIndex);
    const filteredMessage = marked.parse(clearedMessage, {renderer: filterRenderer});

    const referenceDate = {instant: messageCreationTime, timezone: null};

    const parsedTimes = chrono.parse(filteredMessage, referenceDate, {forwardDate: true});
    if (!parsedTimes || !parsedTimes.length) {
        return message;
    }

    // Remove the extra "-"
    let newMessage = charIndex === -1 ? clearedMessage : clearedMessage.slice(0, charIndex) + clearedMessage.slice(charIndex + 1);

    for (let i = 0, len = parsedTimes.length; i < len; i++) {
        const parsedTime = parsedTimes[i];

        if (!parsedTime.start.isCertain('timezoneOffset')) {
            return newMessage;
        }

        let renderingFormat = DATE_AND_TIME_FORMAT;
        let formattedDisplayDate;

        const currentUserStartDate = moment(parsedTime.start.date()).tz(localTimezone).locale(locale);
        if (!currentUserStartDate.isSame(moment(), 'year')) {
            renderingFormat = DATE_FORMAT;
        } else if (!parsedTime.start.isCertain('day') && !parsedTime.start.isCertain('weekday')) {
            renderingFormat = TIME_FORMAT;
        }

        // If the parsedTime is text only (e.g. tomorrow)
        // it should be Casual_Format
        const hasNumbers = /\d/;
        if (!hasNumbers.test(parsedTime.text)) {
            renderingFormat = CASUAL_FORMAT;
        }

        if (parsedTime.end) {
            const currentUserEndDate = moment(parsedTime.end.date()).tz(localTimezone).locale(locale);
            if (!currentUserEndDate.isSame(moment(), 'year')) {
                renderingFormat = DATE_FORMAT;
            }
            if (currentUserStartDate.isSame(currentUserEndDate, 'day')) {
                formattedDisplayDate = `${currentUserStartDate.format(renderingFormat)} - ${currentUserEndDate.format(TIME_FORMAT + ' ' + ZONE_FORMAT)}`;
            } else {
                formattedDisplayDate = `${currentUserStartDate.format(renderingFormat + ' ' + ZONE_FORMAT)} - ${currentUserEndDate.format(renderingFormat + ' ' + ZONE_FORMAT)}`;
            }
        } else {
            formattedDisplayDate = currentUserStartDate.format(renderingFormat + ' ' + ZONE_FORMAT);
        }

        const {text} = parsedTime;
        newMessage = `${newMessage.replace(text, `\`${text}\` *(${formattedDisplayDate})*`)}`;
    }

    return newMessage;
}
