import {convertTimesToLocal} from './time';

beforeEach(() => {
    jest.useFakeTimers();
});

afterEach(() => {
    jest.useRealTimers();
});

test.each([

    // This is an incorrect test case that demonstrates the issue with this plugin. 10am ET is not 8am pacific time at the refrence time.
    // Additional tests shoudl be written if we find a solution to this.
    {
        test: "Let's meet today at 10am ET",
        expected: "Let's meet `today at 10am ET` *(Wed, Jul 17, 2019 08:00 PDT)*",
    },
])('convertTimesToLocal: "$test"', ({test, expected}) => {
    expect(convertTimesToLocal(test, 1563387832493, 'America/Vancouver', 'en')).toEqual(expected);
});

test.each([

    {
        test: 'https://gitlab.collabora.com/tonyk/blog-posts/-/blob/futex_tony/2021-12_tonky_what-is-futex.md',
        expected: 'https://gitlab.collabora.com/tonyk/blog-posts/-/blob/futex_tony/2021-12_tonky_what-is-futex.md'},
    {
        now: new Date('2021-09-08 02:24:57 +0100'),
        test: 'The game is at 12pm UTC',
        expected: 'The game is `at 12pm UTC` *(13:00 BST)*',
    },
    {
        now: new Date('2021-12-10 14:16:35 +0000'),
        test: 'Last deploy is Thursday at 11:59pm UTC',
        expected: 'Last deploy is `Thursday at 11:59pm UTC` *(Fri, Aug 27 00:59 BST)*',
    },
    {
        now: new Date('2021-12-10 14:16:35 +0000'),
        test: 'Last deploy is at 11:59pm UTC',
        expected: 'Last deploy is `at 11:59pm UTC` *(00:59 BST)*',
    },
    {
        now: new Date('2021-12-02 15:00:56 +0000'),
        test: 'Meeting scheduled for 10am GMT+2',
        expected: 'Meeting scheduled for `10am GMT+2` *(09:00 BST)*',
    },

    // The word 'now' should not be rendered with a local time, although chrono-node can identify it as a time reference
    {
        test: 'now that is surprising',
        expected: 'now that is surprising',
    },

    // The word 'ah' should not be interpreted as a time interval
    {
        test: 'ah that is surprising',
        expected: 'ah that is surprising',
    },

    // Messages where the source timezone is missing should not be localized
    {
        test: 'tomorrow at 1pm',
        expected: 'tomorrow at 1pm',
    },

    // Messages where the casual date (e.g. tomorrow, today...) is separate from the hous+timezone
    {
        now: new Date('2021-08-23 02:24:57 +0000'),
        test: 'tomorrow maybe at 1pm UTC',
        expected: '`tomorrow` *(Tue, Aug 24 BST)* maybe `at 1pm UTC` *(14:00 BST)*',
    },

    // Mentioning a weekday name should, by default, refer to the next occurrence of that weekday
    {
        now: new Date('2021-12-02 15:00:56 +0000'),
        test: 'Sunday at 4pm GMT',
        expected: '`Sunday at 4pm GMT` *(Sun, Aug 29 17:00 BST)*',
    },

    // Workaround to prevent converting the word "get" as Georgia Standard Time
    {
        now: new Date('2021-12-02 15:00:56 +0000'),
        test: 'I now get an error when trying to fix the bug.',
        expected: 'I now get an error when trying to fix the bug.',
    },

    // Converting the word "get" to Georgia Standard Time only when it is uppercase
    {
        now: new Date('2021-12-02 15:00:56 +0000'),
        test: 'At 14 GET, could pick up the cake on the bakery, please?',
        expected: '`At 14 GET` *(11:00 BST)*, could pick up the cake on the bakery, please?',
    },
    {
        now: new Date('2021-08-25 20:24:57 +0000'),
        test: 'today any time before 17:00 CEST - tomorrow before 17:00 CEST',
        expected: '`today` *(Mon, Aug 23 BST)* any time before `17:00 CEST` *(16:00 BST)* - `tomorrow before 17:00 CEST` *(Tue, Aug 24 16:00 BST)*',
    },
    {
        now: new Date('2021-08-25 20:24:57 +0000'),
        test: 'today 17:00 - 18:00 CEST',
        expected: '`today 17:00` *(Mon, Aug 23 16:00 BST)* - `18:00 CEST` *(17:00 BST)*',
    },
])('timezoneParsing: "$test"', ({now, test, expected}) => {
    // Some of the library's logic is currently not 'stable', because, for example, we call moment() without arguments
    // A few of our test cases are therefore pinned to specific points-in-time, pending temporal stability
    if (now) {
        jest.setSystemTime(now);
    }
    expect(convertTimesToLocal(test, 1629738610000, 'Europe/London', 'en')).toEqual(expected);
});

test('crossDaylightSavings', () => {
    const testCases = [
        {
            test: 'Today from 2pm - 7pm PDT',
            expected: '`Today from 2pm` *(Sat, Oct 30 22:00 BST)* - `7pm PDT` *(02:00 GMT)*',
        },
    ];

    testCases.forEach((tc) => {
        jest.setSystemTime(new Date(1638965948000));
        expect(convertTimesToLocal(tc.test, 1635562800000, 'Europe/London', 'en')).toEqual(tc.expected);
    });
});

test('codeBlocks', () => {
    const testCases = [
        {
            test: '> 12:00 GMT',
            expected: '> 12:00 GMT',
        },
        {
            test: '`12:00 GMT`',
            expected: '`12:00 GMT`',
        },
        {
            test: '```12:00 GMT```',
            expected: '```12:00 GMT```',
        },
        {
            test: '```sometext\n12:00 GMT```',
            expected: '```sometext\n12:00 GMT```',
        },
        {
            test: 'https://example.com?date=2023-01-01T10:00',
            expected: 'https://example.com?date=2023-01-01T10:00',
        },
        {
            test: '[https://example.com?date=2023-01-01T10:00](test)',
            expected: '[https://example.com?date=2023-01-01T10:00](test)',
        },
        {

            // Example of edge case not being handled correctly ATM. Since it performs a simple .replace,
            // whatever is the first match will be converted, regardless of it being a code block or not.
            test: '`text with time 12:00 GMT`\nfollowed by block with time 12:00 GMT',
            expected: '`text with time `12:00 GMT` *(13:00 BST)*`\nfollowed by block with time 12:00 GMT',
        },
    ];

    testCases.forEach((tc) => {
        jest.setSystemTime(new Date(1638965948000));
        expect(convertTimesToLocal(tc.test, 1635562800000, 'Europe/London', 'en')).toEqual(tc.expected);
    });
});